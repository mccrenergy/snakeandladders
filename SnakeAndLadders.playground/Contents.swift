//: [Previous](@previous)
/*:
# Assignment: Snake & Ladders
Now that you've got some idea how Swift looks like, it's time for you to get your hands dirty doing the first Swift assignment.

Build a simple Snake & Ladders game. You'll need to have some game setup function, which keeps a board of 100 squares. Some of the squares are "Snakes", meaning that a player who lands on the square will fall back a number of positions. Some of the squares are "Ladders", where a player who lands there will go forward a number of positions. The goal, is to be the first to reach the 100th square. You can set the number of players as a variable, anything between 2 to 5 makes a good number.

The main loop will basically roll the dice for each player, and print out the their current position, rolled dice, new position, and effect of landing on a snake or ladder.

No user input is expected for this simple assignment. The program just has to keep on running until one player reaches the goal.
*/
import Foundation

// Initial variables (just to get you started)

class Player {
    var name : String
    var position : Int
    
    init(name: String) {
        self.name = name
        self.position = 0
    }
}


var player1: Player = Player(name: "PJ")
var player2: Player = Player(name: "TO")

player1.name = "PJ"
player2.name = "TO"

var players : [Player] = [player1, player2]

var board: [Int] = []
// Functions

func boardSetup() {
    board = [Int](count: 100, repeatedValue: 0)
    
    board[03] = +08; board[06] = +11; board[09] = +09; board[10] = +02
    board[14] = -10; board[19] = -11; board[22] = -02; board[24] = -08
    board[30] = -04; board[39] = +08; board[42] = +02; board[44] = -05
    board[49] = -10; board[50] = +07; board[59] = -12; board[73] = +06
    board[79] = +05; board[95] = -09;
}

func rollDice() -> Int{
    return Int(rand()%6) + 1
}

boardSetup()
var hasWinner: Bool = false
//
//repeat{
//
//}while !hasWinner

while (hasWinner == false) {
    // For each player, find the current position, roll the dice,
    // set the new position based on the board's snakes and ladders
    // print out the move
    //rollDice()
    
    boardSetup()
    
    for player in players {
        
        let diceValue : Int = rollDice()
        
        if player.position < 100
        {
            let boardValue = Int(board[player.position].value)
            player.position = player.position + diceValue + boardValue
        
            var boardValueMessage : String = boardValue < 0 ? " \(player.name) step on a snake and pushed back for \(abs(boardValue)) step !":boardValue > 0 ? " \(player.name) step on a ladder and pushed forward for \(abs(boardValue)) step !":""
            
            
            
            print ("\(player.name) rolled \(diceValue) and arrive at position \(player.position)." + boardValueMessage)
        }
        
        if player.position > 99
        {
            hasWinner = true
            print ("The winner is \(player.name) !")
            break
        }
        
    }
    
}
